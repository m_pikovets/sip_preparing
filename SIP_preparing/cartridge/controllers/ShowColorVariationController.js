'use strict';

/* API Includes */
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');
var ISML = require('dw/template/ISML');

/* Script Modules */
var app = require('siteGenesis_controllers/cartridge/scripts/app');
var guard = require('siteGenesis_controllers/cartridge/scripts/guard');


/**
 * Print color variants for product.
 */
function show() {
	var currentHttpParameterMap = request.httpParameterMap;
	var Product = dw.catalog.ProductMgr.getProduct(currentHttpParameterMap.pid.value);
	
	if (Product == null) {
		Logger.error(Resource.msgf('productnotfoundwithid.message', 'product', null, currentHttpParameterMap.pid.value));
		
		app.getView().render('ShowColorVariation/err');
		
		return false;
	}
	
	if (Product.master) {
		var varModel = Product.getVariationModel();
		var varAttrsColor = Product.variationModel.getProductVariationAttribute('color');
		
		var selectableColors = new dw.util.ArrayList();
		if( varAttrsColor != null ) {
			var allColors = varModel.getAllValues(varAttrsColor);
			var test = allColors.size();
			
			for (var i = 0; i < allColors.size(); i++) {
                var item = allColors[i];

                if(varModel.hasOrderableVariants(varAttrsColor, item)) {
					selectableColors.add( item );
				}
            }
			
			
		}
	}

	ISML.renderTemplate('ShowColorVariation/product', {Product: Product, selectableColors: selectableColors});

}

/** @see module:controllers/ShowColorVariationController~show */
exports.Show = guard.ensure(['get'], show);
