'use strict';

/* API Includes */
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');
var ISML = require('dw/template/ISML');

/* Script Modules */
var app = require('siteGenesis_controllers/cartridge/scripts/app');
var guard = require('siteGenesis_controllers/cartridge/scripts/guard');
var Category = app.getModel('Category');
var Compare = app.getModel('Compare');


/**
 * Print color variants for product.
 */
function show() {
	var currentHttpParameterMap = request.httpParameterMap;
	var pdict = {};
	
	// get all products
	var category = Category.get('electronics-televisions-projection');
	var electronicsProducts = category.getProducts();
	var electronicsProductsCount = electronicsProducts.size();
	
	// pricing
	var product = dw.catalog.ProductMgr.getProduct(currentHttpParameterMap.pid.value);
	var priceModel = product.getPriceModel(); 
	var minPrice = priceModel.minPrice;
	
	// categories
	var categoriesProduct = product.getAllCategories();
	
	// promotions
	var promotions = dw.campaign.PromotionMgr.getCampaigns();
	
	// lineItems
//	var lineItems = dw.order.LineItemCtnr.getAllLineItems();
	
	pdict.categoriesProduct = categoriesProduct;
	pdict.product = product;
	
	ISML.renderTemplate('CodeTest/main', pdict);
	

}

/** @see module:controllers/CodeTest~show */
exports.Show = guard.ensure(['get'], show);
