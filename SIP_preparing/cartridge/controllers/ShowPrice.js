'use strict';

/* API Includes */
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');
var ISML = require('dw/template/ISML');

/* Script Modules */
var app = require('siteGenesis_controllers/cartridge/scripts/app');
var guard = require('siteGenesis_controllers/cartridge/scripts/guard');


/**
 * Print price for product.
 */
function show() {
	var currentHttpParameterMap = request.httpParameterMap;
	var Product = dw.catalog.ProductMgr.getProduct(currentHttpParameterMap.pid.value);
	
	if (Product == null) {
		Logger.error(Resource.msgf('productnotfoundwithid.message', 'product', null, currentHttpParameterMap.pid.value));
		
		app.getView().render('ShowColorVariation/err');
		
		return false;
	}else {
//		debug template
//		if (Product.master && Product.priceModel.isPriceRange()) {
//			var minPrice = Product.priceModel.minPrice;
//		}else if (Product.master && !Product.priceModel.isPriceRange()) {
//			Product = Product.variationModel.variants[0];
//		}
		ISML.renderTemplate('showPrice/price', {Product: Product});
	}

	

}

/** @see module:controllers/ShowPrice~show */
exports.Show = guard.ensure(['get'], show);
