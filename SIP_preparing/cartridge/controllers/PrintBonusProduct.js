'use strict';

/* API Includes */
var Resource = require('dw/web/Resource');
var Logger = require('dw/system/Logger');
var ISML = require('dw/template/ISML');

/* Script Modules */
var app = require('siteGenesis_controllers/cartridge/scripts/app');
var guard = require('siteGenesis_controllers/cartridge/scripts/guard');


/**
 * Print price for product.
 */
function show() {
	var currentHttpParameterMap = request.httpParameterMap;
	var pdict = {};
	var promoPlan = dw.campaign.PromotionMgr.getActiveCustomerPromotions();
	var orderPromotions = promoPlan.getOrderPromotions();
	var promo = null,
		promoCallout = null,
		promoDetails = null,
		basket = dw.order.BasketMgr.getCurrentBasket(),
		lineItems = null,
		productsInBusket = [];
	
	if (basket) {
		lineItems = basket.allLineItems;
		
		for(var u = 0; u < lineItems.length; u++){
			if ('product' in lineItems[u]) {
				productsInBusket.push(lineItems[u].product);
			}
		}
	}
	
	for(var i=0; i<orderPromotions.length; i++){
		promo = orderPromotions[i];
		promoCallout = promo.getCalloutMsg().getMarkup();
		promoDetails = promo.getDetails().getMarkup();
	}
	
	pdict.promoDetails = promoDetails;
	pdict.promoCallout = promoCallout;
	pdict.productsInBusket = productsInBusket;
	
	
	
	
	if (pdict.promoCallout && pdict.promoDetails) {
		ISML.renderTemplate('PrintBonusProduct/template', pdict);
		
		return false;
	}else {
		
		Logger.error(Resource.msgf('productnotfoundwithid.message', 'product', null));
		
		return true;
	}

	

}

/** @see module:controllers/ShowPrice~show */
exports.Show = guard.ensure(['get'], show);
