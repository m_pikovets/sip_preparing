'use strict';

var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var ISML = require('dw/template/ISML');
let Pipeline = require('dw/system/Pipeline');



function startPipeline() {

	let pdict = Pipeline.execute('Home-Show', { MyArgBoolean: true });
	
	ISML.renderTemplate('test/test', {error: false});
}


/** @see module:controllers/Page~include */
exports.StartPipeline = guard.ensure(['get'], startPipeline);
